Bug 01
1. Press "New Record" button
2. before clicking "OK", press the "delete record" button
3. What happens is that a record gets deleted BEFORE a new record was ever written to the database. This results in the first entry of the database being deleted instead of the unfinished new record.
# Currently patched up by removing the "onclick" function of the "Delete" button while in new-record-creation mode. (line 99 in script.js - the insertRow() function)

Bug 02
1. The page looks horrendous in IE

[Fixed] Bug 03
1. The "Edit" and "Delete" buttons of inactive record rows are not well centered in their row (horizontally) on larger screens

Bug 04
1. The "New Record" button is not correctly positioned and escapes its usual position on very small screens or when there is a long list of records (so long that you need to scroll down).

Bug 05
1. When editing an existing record, the text jumps up with 1 or 2 px when you select the field. I think it's because of "box-sizing: border-box".

Bug 06
1. Pressing Enter in the search field submits the form and resets the page.

[Fixed] Bug 07
1. Filtering and Search functions override each other, because each of them loops through ALL rows on the page, disregarding the current value of their "display" propery. They should take that into account.

[Fixed] Bug 08
  1. Open the application.
  2. Click on any record row to make it active
  3. Click "Edit" - everything works fine.
  4. Use some of the filters and then clear any applied filters so that you see all records again.
  5. Select any record again, and click on its "Edit" button.
  6. It displays incorrectly. The reason is that when the filters change the display property of the elements, the style becomes inline. This results in the CSS stylesheet being overriden by the display property applied inline by the last filter. Not good.

[Fixed] Bug 09
1. If you apply any filter, all of the records disappear, even though they might satisfy the filter criteria.
2. ONLY IF you apply a DATE filter FIRST, BEFORE applying other filters, then everything works as expected.